from pysci import rnaseq

ddf_abundance = rnaseq.kallisto_h2df("/home/exacloud/lustre1/HeiserLab/patterja/tatlow_BCCL/output/")
rnaseq.abundance2tsv(ddf_abundance, s_prefix="bcclRnaseqV3")
