####
# title: mema_norm_to_rdataframe.py
#
# language: python3
# license: GPLv>=3
# date: 2019-08-26
# author: bue
#
# run: python3 mema_norm_to_rdataframe.py
#
# description:
#     glue datafrane for analysis together
####

# library
import os
import pandas as pd
from pymema import run

# const
s_study = "hcc1143_highserum_sst" #os.getcwd().split("/")[-1]
s_mema_ac = "/home/exacloud/tempwork/HeiserLab/buchere/data_mema/hcc1143_highserum_sst/mema_ac/"
s_mema_level0 = "/home/exacloud/tempwork/HeiserLab/buchere/data_mema/hcc1143_highserum_sst/mema_level0/"
s_opath = "/home/exacloud/tempwork/HeiserLab/buchere/output/df_glued/"
os.makedirs(s_opath, exist_ok=True)


# function
def glue_segement_df_cell():
    """
    description:
        glues guillaumes mesurement
    """
    print(f"\nglue_segement_df_cell ...")
    df_glued_study = pd.DataFrame()

    # for each run
    for s_file in sorted ([s_file for s_file in os.listdir(s_mema_level0) if s_file.endswith("_imageIDs.tsv")]):
        s_run = s_file.split("_")[0]
        s_runid = f"mema-{s_run}"
        print(f"glue_segement_df_cell process {s_file} ...")

        # load perturbation annotation
        df_assay_coor = run.perturbation_antsv2df(f"{s_mema_ac}{s_run}_tidy_perturbation.tsv.gz")

        # load imageid to spot mapping
        df_coor_image = pd.read_csv(f"{s_mema_level0}{s_file}", sep="\t")
        # get coor
        df_coor_image.sort_values(["WellName","Row","Column"], inplace=True)
        df_coor_image.reset_index(inplace=True)
        df_coor_image["coor"] = df_coor_image.index + 1
        # extract relevanty columns
        df_coor_image = df_coor_image.loc[:,["coor","ImageID"]]

        # load guillaume segmentation output
        df_image_segment = pd.DataFrame()
        for s_well in [
                "_Well_1_level_0.csv",
                "_Well_2_level_0.csv",
                "_Well_3_level_0.csv",
                "_Well_4_level_0.csv",
                "_Well_5_level_0.csv",
                "_Well_6_level_0.csv",
                "_Well_7_level_0.csv",
                "_Well_8_level_0.csv",
            ]:
            try:
                df_image_segment = df_image_segment.append(
                    pd.read_csv(f"{s_mema_level0}{s_file.replace('_imageIDs.tsv',s_well)}")
                )
            except FileNotFoundError:
                print(f"Warning: {s_file.replace('_imageIDs.tsv',s_well)} does not exit (which might be ok for this run or might not).")

        # run fusion
        df_coor_image_segement = pd.merge(df_coor_image, df_image_segment, on="ImageID")
        df_glued_run = pd.merge(df_assay_coor, df_coor_image_segement, on="coor")
        df_glued_run.rename({"Label": "CellID"}, axis=1, inplace=True)

        # handle index
        df_glued_run.index = df_glued_run.apply(lambda n: f"{n.runid}-{n.coor}-{n.CellID}", axis=1)
        df_glued_run.index.name = "index"
        df_glued_run.sort_index(axis=0, inplace=True)

        # study fusion
        df_glued_study = df_glued_study.append(df_glued_run, verify_integrity=True)

    # write to file
    print(f"glue_segement_df_cell zipping result ...")
    df_glued_study.to_csv(f"{s_opath}{s_study}_df_segmentation.tsv.gz", sep="\t", compression="gzip", header=True)


# main call
if __name__ == '__main__':

    # run dmc
    glue_segement_df_cell()
