import pandas as pd
from pysci import rnaseq


## get kallisto compatible gene level datafile
ds_file = {
    "count": f"GTEx_Analysis_2016-01-15_v7_gencode.v24.kallisto.fake_transcript_count.tsv.gz",
    "tpm": f"GTEx_Analysis_2016-01-15_v7_gencode.v24.kallisto.fake_transcript_tpm.tsv.gz",
}
ddf_wg = rnaseq.tsv2abundance(ds_file=ds_file, sep="\t", index_col=0)
# summarize to gene level
ddf_wg = rnaseq.trans2gene(ddf_wg)
# write to files
rnaseq.abundance2tsv(ddf_wg, s_prefix=f"gtex_rnaseqGeneRaw_wgBue")
