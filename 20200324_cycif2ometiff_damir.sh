#!/bin/bash

spacing=0.325

while getopts "p:lv" opt; do
    case $opt in
        p)
            echo "pixel spacing will be: $OPTARG" >&2
            spacing=$OPTARG
            ;;
        l)
            echo "Legacy OME-TIFF output is enabled" >&2
            legacy=1
            ;;
        v)
            echo "Verbose mode is enabled" >&2
            verbose=1
            ;;
        \?)
            echo "Invalid option -$OPTARG" >&2
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument" >&2
            exit 1
            ;;
     esac
done
shift "$(($OPTIND -1))"

anyopts="-p ${spacing} "
if [[ $legacy = 1 ]]; then anyopts="${anyopts} -l "; fi
if [[ $verbose = 1 ]]; then anyopts="${anyopts} -v "; fi

for arg in "$@"
do
	echo "Processing directory: ${arg}"
	name=`basename ${arg}`
	[ ! -d "$arg/OMETIFF" ] && mkdir ${arg}/OMETIFF
	python3 /usr/local/bin/cycif2ometiff.py ${anyopts} ${arg} ${arg}/OMETIFF/${name}
	echo "Done with: ${arg}"
done

echo "Done with all"

