exacloud
========

shh exacloud.ohsu.edu
cd /home/exacloud/tempwork/HeiserLab/buchere/


pwm source code at:
/home/exacloud/tempwork/HeiserLab/buchere/src/

group wide software at:
#/home/exacloud/tempwork/HeiserLab/bin/
#/home/exacloud/tempwork/HeiserLab/src/


# notes
+ python3 installed with minicoda because of the c librarires
+ cellranger uses star aligner


# commands
srun   # run adhoc
sbatch  # run batch
squeue | grep -i buchere
tail -f file.err


# cellranger
https://support.10xgenomics.com/single-cell-gene-expression/software/pipelines/latest/what-is-cell-ranger
https://support.10xgenomics.com/single-cell-gene-expression/software/pipelines/latest/installation
srun cellranger sitecheck > cellranger_sitecheck.txt
srun cellranger testrun --id=tiny > cellranger_testrun_srun.txt

